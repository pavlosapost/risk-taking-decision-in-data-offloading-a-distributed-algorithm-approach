function [ri] = rate_of_return(fi,fc,vi,Li,Lt,P,Ri,bt,bth,ai)
r = rate(Li,Lt,bt,bth,fc);
if(r == 0)
    ri = 0;
else
    x = (Li/fi + Li*vi - (P+1)/(Ri) - Li/r);
   % x = (1/fi + vi - (P+1)/Ri - 1/r);
    if(x<0)
        ri = 0;
    else
        ri = power(x,ai);
    end
end
end

