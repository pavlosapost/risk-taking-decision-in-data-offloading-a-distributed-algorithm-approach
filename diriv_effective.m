function [value] = diriv_effective(fi,fc,vi,Li,Lt,P,Ri,bt,bth,ai,ki)

dir_rate = diriv_rate(fi,fc,vi,Li,Lt,P,Ri,bt,bth,ai);
rate_return = rate_of_return(fi,fc,vi,Li,Lt,P,Ri,bt,bth,ai);
prob = probability(bt,bth);
x = (P+1)/Ri;
ei = power(x,ai);

value = dir_rate * (1-prob) - rate_return/bth - ki*ei/bth;

end

