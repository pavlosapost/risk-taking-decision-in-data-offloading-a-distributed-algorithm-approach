function [index] = binarySearch(num,fi,fc,vi,Li,Lt,P,Ri,b_i,bth,ai,ki)

%--------------------------------------------------------------------------
% Syntax:       [index] = binarySearch(A, n, num);
%               
% Inputs:       A: Array (sorted) that you want to search
%               n: Length of array A
%               num: Number you want to search in array A
%               
% Outputs:      index: Return position in A that A(index) == num
%                      or -1 if num does not exist in A
%               
% Description:  This function find number in array (sorted) using binary
%               search
%               
% Complexity:   O(1)    best-case performance
%               O(log_2 (n))    worst-case performance
%               O(1)      auxiliary space
%               
% Author:       Trong Hoang Vo
%               hoangtrong2305@gmail.com
%               
% Date:         March 31, 2016
%--------------------------------------------------------------------------

left = 1;
right = bth;
flag = 0;

while left <= right
    mid = (left + right) / 2;
    %mid = ceil((left + right) / 2);
    bt = mid + b_i;
    if(bt>=bth)
        right = mid - 1;
        if(right == 0)
            index = 0;
            flag = 1;
            break;
        else
            continue;
        end
    end
    x = diriv_effective(fi,fc,vi,Li,Lt,P,Ri,bt,bth,ai,ki);
    y = effective_rate_of_return(fi,fc,vi,Li,Lt,P,Ri,bt,bth,ai,ki);
    value = mid*x + ai*y;
    if (abs(value) <= num)
        index = mid;
        flag = 1;
        break;
    else
        if (value > 0)
            %right = mid - 1;
            left = mid + 1;
        else
            right = mid - 1;
            %left = mid + 1;
        end
    end
end

if flag == 0
    index = -1;
end

end

