function [eri] = effective_rate_of_return(fi,fc,vi,Li,Lt,P,Ri,bt,bth,ai,ki)
prob = probability(bt,bth);
x = (P+1)/Ri;
ei = power(x,ai);

if(prob<1)
    value = rate_of_return(fi,fc,vi,Li,Lt,P,Ri,bt,bth,ai);
    eri = value*(1-prob)-ki*ei*prob;
else
    eri = -ki*ei;
end
end

