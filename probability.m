function [p] = probability(bt,bth)
    if(bt>=bth)
        p = 1;
    else
        p = bt/bth;
    end
end

