load('results_ki_5_binary');

%% Bar Chatc
figure();

x = 1:1:3;
factor = [Li(7)*Ii(7)/(di(7)*fi(7)) Li(42)*Ii(42)/(di(42)*fi(42)) Li(6)*Ii(6)/(di(6)*fi(6))];
b = bar(diag(factor), 'stacked');
set(gca, 'xticklabel', {'User 7', 'User 42', 'User 6'});
%ylabel('Factor: $\mathbf{\frac{\lambda_i*I_i}{d_i*f^l_i}}$', 'Interpreter', 'latex');
ylabel('Factor: ^{L_i * I_i}/_{d_i * F_i^l}');

% h = bar(x(1),factor(1));
% set(h, 'FaceColor', [0.6350 0.0780 0.1840]);
% h = bar(x(2),factor(2));
% set(h, 'FaceColor', [0.8500 0.3250 0.0980]);
% h = bar(x(3),factor(3));
% set(h, 'FaceColor', [0 0.4470 0.7410]);
annotation('textbox',...
    [0.15 0.65 0.3 0.15],...
    'String',{'d_i: Distance', 'F_i^l: Local computation capability', 'I_i: Input Bits', 'L_i: Computation Intensive Parameter'},...
    'FontSize',14,...
    'FontName','Arial',...
    'LineStyle','--');...
%     'EdgeColor',[1 1 0],...
%     'LineWidth',2,...
%     'BackgroundColor',[0.9  0.9 0.9],...
%     'Color',[0.84 0.16 0]);

  %  'String',{'$\mathbf{d_i}$:Distance', '$\mathbf{F_i^l}$:Local computation capability','$\mathbf{I_i}$:Input Bits', '$\mathbf{\lambda_i}$:Computation Intensive Parameter'},...
    %'Interpreter','latex',...

%% Bt-Bi vs Iterations

temp(1:ite) = bth;
figure(); 
axes('box','on'); 
hold on;  
yyaxis left 
plot(1:ite,Bt,'o-c','LineWidth',1.5,'MarkerSize',6);
plot(1:ite,temp,'o-y','LineWidth',1.5,'MarkerSize',6);
ylabel('Total Offloading'); 
yyaxis right
plot(1:ite,Bi(6,:),'d--k','LineWidth',1.5,'MarkerSize',6);
plot(1:ite,Bi(42,:),'d--r','LineWidth',1.5,'MarkerSize',6);
plot(1:ite,Bi(7,:),'d--b','LineWidth',1.5,'MarkerSize',6);
ylabel('Users Offloading'); 
grid on; 
hold off; 
set(gca,'FontWeight','bold');

%% Avg_Ei - Avg_Oi vs Iterations
�

%% Probability of Failure