function [ov] = overhead(i,Li,Ii,bi,Ri,fi,vi,bt,bth,fc)
L = sum(Li(:));
temp = (1-bt/bth)*fc;
ov = bi(i)*(L/temp + (P+1)/Ri) + Li(i)*(Ii(i)-bi(i))*(1/fi(i) + vi);
end

