%% Setting Parameters
D = 500;
%% Distances and Channel Gains
di = zeros(1,D);
gi = zeros(1,D);
for i = 1:D
    a = 0.5; b = 50;
    di(i) = (b-a)*rand + a;
    gi(i) = 1/(di(i)^4);
end
% 
%% Local computation capability into Cycles/sec
fi = zeros(1,D);
for i = 1:D
    temp = randi([1 10],1,1);
    fi(i) = temp*10^8;
end
% 
%% Local energy coefficient
vi = zeros(1,D);
for i = 1:D
    vi(i) = 2.5*10^(-9)*fi(i)^2;
end
% 
%% Input Data into KBytes
Ii = zeros(1,D);
for i = 1:D
    temp = randi([200 2000],1,1);
    Ii(i) = temp * 10^3;
end

%% Cycles
% Cycles = zeros(1,D);
% for i = 1:D % MegaCycles
%     Cycles(i) = randi([1000 2000],1,1)*10^6;
% end

%% Learning Parameter
% for i = 1:D
%     Li(i) = Cycles(i)/Ii(i);
% end
%% Application Parameter
Li = zeros(1,D);
for i = 1:D
    a = 1; b = 5;
    temp = (b-a)*rand + a;
    Li(i) = temp*10^3;
end

%% Initialization Offloading
bi_input = zeros(D,1);
for i = 1:D
    bi_input(i) = randi([0 Ii(i)],1,1);
end

% %% other channel gains
% P = zeros(1,D);
% for i = 1:D
%     P(i) = 0.1 * (di(i)^2/50^2);
% end
% 
% 
% for i = 1:D
%     gi(i) = gi(i)*di(i)^2;
%     vi(i) = 10^(-9);
% end



%% Loading Inputs and Setting number of Devices
clear all;clc;
load('inputs_last.mat');
D = 50;

vi(1:D) = 10^(-9);

%% Scalability Matrices
Iterations = [];
Bt_Devices = [];
Ei_Devices = [];
Oi_Devices = [];
Pr_Devices = [];

%% Ki_Matrices
Ki_Iterations = [];
Ki_Bt_Devices = [];
Ki_Ei_Devices = [];
Ki_Oi_Devices = [];
Ki_Pr_Devices = [];

%% Main Program Respects to Devices
%D = 50; % devices

% MEC Server's computation capability
fc = 1000*10^9;

%% Thresshold for the MEC server

bth = 0.09*sum(Ii(1:50));
%% Communication Parameters
% bandwidth
w = 5*10^6;
% background noise
s0 = 10^(-13);

% power of each device
%P = 10^(-4);

%% Prospect theoretic Parameters
% Homogeneous Players
ai = 0.2; ki = 5;
Lt = sum(Li(1:D));

%% first amount of offloading data randomly
bi = zeros(D,1);
bi(1:D) = bi_input(1:D);


%% expected utility matrix
ei = zeros(D,1);
%% overhead matrix
oi = zeros(D,1);
%% time overhead matrix
oi_time = zeros(D,1);
%% energy overhead matrix
oi_energy = zeros(D,1);

%% minimum transmission rate overall
R_min = 10^(30);
%Rserv = 64*10^3;
Rserv = 5*10^6/(128*10^3);
%Rserv = 1;
test_value = 10^(-8);

%% PNE
conv = false;
ite = 0;
Bi = []; Pr=[]; Ei = []; Oi=[]; Oi_Time=[]; Oi_Energy=[]; Bt=[];
des_value = 1;
est = 10^(-4);
while (conv == false)
    ite = ite+1; ite   
    %% Sequential Best Response Dynamics
    for i = 1:D

        %% Interference - Transmission Rate
        I = 0;
        L_acc = 0;
        for u = 1:D
            if(bi(u)~=0 && u~=i)
                I = I + P(u)*gi(u);
                L_acc = L_acc + Li(u);
            end
        end
        if(L_acc == 0)
            Lt = sum(Li(:));
        else
            Lt = L_acc;
        end
        Ri = w*log(1+Rserv*(P(i)*gi(i))/(s0+I));
     
        %% Check the aggregate investment of the rest
        b_i = sum(bi(:)) - bi(i);
        value = effective_rate_of_return(fi(i),fc,vi(i),Li(i),Lt,P(i),Ri,b_i,bth,ai,ki);
        if(value<=0)
            bi(i)=0;
        else
            index_root = binarySearch(est,fi(i),fc,vi(i),Li(i),Lt,P(i),Ri,b_i,bth,ai,ki);
            if(index_root == -1)
                testt = 1000;
            else
                bi(i) = min([index_root,Ii(i)]);
                bi(i) = ceil(bi(i));
            end
        end
        %% Expected Utility
        if(bi(i) == 0)
            ei(i) = 0;
        else
            ei_eff = effective_rate_of_return(fi(i),fc,vi(i),Li(i),Lt,P(i),Ri,sum(bi(:)),bth,ai,ki);
            ei(i) = power(bi(i),ai)*ei_eff;
        end
        prob = probability(sum(bi(:)),bth);
        %% Expected Overhead
        if(sum(bi(:))>= bth)
            oi(i) = Li(i)*Ii(i)*(1/fi(i) + vi(i)) + bi(i)*((P(i)+1)/Ri);
            oi_time(i) = Li(i)*Ii(i)/fi(i) + bi(i)/Ri;
            oi_energy(i) = Li(i)*Ii(i)*vi(i) +  bi(i)*P(i)/Ri;
        else    
            oi_rate = rate(Li(i),Lt,sum(bi(:)),bth,fc);
            %oi(i) = bi(i)*(Li(i)/oi_rate + (P+1)/Ri) + Li(i)*(Ii(i)-bi(i))*(1/fi(i) + vi(i));
            oi(i) = (bi(i)*(Li(i)/oi_rate + (P(i)+1)/Ri) + Li(i)*(Ii(i)-bi(i))*(1/fi(i) + vi(i)))*(1-prob) + prob*(Li(i)*Ii(i)*(1/fi(i) + vi(i)) + bi(i)*((P(i)+1)/Ri));
            oi_time(i) = (1-prob)*(bi(i)*Li(i)/oi_rate + bi(i)/Ri + Li(i)*(Ii(i)-bi(i))/fi(i)) + prob*(Li(i)*Ii(i)/fi(i) + bi(i)/Ri);
            oi_energy(i) = (1-prob)*(bi(i)*P(i)/Ri + Li(i)*(Ii(i)-bi(i))*vi(i)) + prob*(Li(i)*Ii(i)*vi(i) + bi(i)*P(i)/Ri);
        end
    end
    %% Store Best Strategies, Total Investment, Probability of Failure
    Bi = [Bi bi]; % best strategies
    Bt = [Bt sum(bi(:))]; % total investment
    Ei = [Ei ei];
    Oi = [Oi oi];
    Oi_Time = [Oi_Time oi_time];
    Oi_Energy = [Oi_Energy oi_energy];
    Pr = [Pr probability(sum(bi(:)),bth)]; % probability of failure
    %% Check Convergence to PNE
    if(ite>1)
        summ = 0;
        for u = 1:D
            if(Bi(u,ite-1)==Bi(u,ite))
                summ = summ + 1;
            end
        end
        if(summ == D)
            conv = true;
        end
    end
end

%% Average Expected Utility
Avg_Ei = zeros(1,ite);
for i = 1:ite
    Avg_Ei(i) = sum(Ei(:,i))/D;
end

%% Average Expected Overhead
Avg_Oi = zeros(1,ite);
for i = 1:ite
    Avg_Oi(i) = sum(Oi(:,i))/D;
end

%% Average Expected Time Overhead
Avg_Oi_Time = zeros(1,ite);
for i = 1:ite
    Avg_Oi_Time(i) = sum(Oi_Time(:,i))/D;
end

%% Average Expected Energy Overhead
Avg_Oi_Energy = zeros(1,ite);
for i = 1:ite
    Avg_Oi_Energy(i) = sum(Oi_Energy(:,i))/D;
end



%% Average Local Overhead
Local_Overhead = zeros(1,D);
for i = 1:D
    Local_Overhead(i) = Li(i)*Ii(i)*(1/fi(i) + vi(i));
end
%Avg_Local_Overhead = mean(Local_Overhead);

%% Referenced Average Overhead
Oi_Relevant = zeros(D,ite);
for i = 1:ite
    for j = 1:D
        Oi_Relevant(j,i) = Oi(j,i) / Local_Overhead(j);
    end
end

Avg_Oi_Relevant = zeros(1,ite);
for i = 1:ite
    Avg_Oi_Relevant(i) = mean(Oi_Relevant(:,i));
end







